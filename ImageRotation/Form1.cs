﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace ImageRotation
{
    public partial class Form1 : Form
    {
        Image image;
        int angle;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            
            image = Image.FromFile(openFileDialog1.FileName);
            Application.OpenForms["Form1"].BringToFront();
            
        }


        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            angle =  (int)numericUpDown1.Value;
            Invalidate();
        }

       

        private void Form1_Load(object sender, EventArgs e)
        {
            MessageBox.Show("Insert a photo to rotate", "Gibe S. Tirol", MessageBoxButtons.OK, MessageBoxIcon.Information);
            openFileDialog1.ShowDialog();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void btnOpen_Paint(object sender, PaintEventArgs e)
        {

        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            //Corner should not be cropped
            int newWidth = 0;
            int newHeight = 0;
            Bitmap bmp = new Bitmap(image.Width, image.Height);

            var res = ReturnNewWidthNewHeight(angle, newWidth, newHeight, bmp);
            newWidth = res.Item1;
            newHeight = res.Item2;
            this.BackColor = Color.FromArgb(res.Item3, res.Item4, res.Item5);           

            Bitmap bitmap = new Bitmap(newWidth, newHeight);
            Graphics graphics = Graphics.FromImage(bitmap);

            //roate the image
            graphics.TranslateTransform(newWidth / 2, newHeight / 2);
            graphics.RotateTransform(angle);
            graphics.TranslateTransform(-image.Width / 2, -image.Height / 2);
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //display and center the image
            graphics.DrawImage(image, 0, 0);
            e.Graphics.TranslateTransform(this.Width / 2, this.Height / 2);
            e.Graphics.DrawImage(bitmap, -bitmap.Width/2, -bitmap.Height/2);
                                 
        }
        
        static Tuple<int, int, int, int, int>ReturnNewWidthNewHeight(int angle, int newWidth, int newHeight, Bitmap bmp)
        {
            var y_coor = 2 * Math.PI * angle / 360;
            var x_coor = 2 * Math.PI * angle / 360;
            int red, green, blue;
            red = green = blue = 0;
            if (angle <= 90)
            {
                newWidth = (int)(bmp.Width * Math.Cos(x_coor) + bmp.Height * Math.Sin(y_coor));
                newHeight = (int)(bmp.Height * Math.Cos(x_coor) + bmp.Width * Math.Sin(2 * Math.PI * angle / 360));
                red = 204; blue = 204; green = 255;
            }
            else if (angle > 90 && angle <= 180)
            {
                newWidth = (int)(bmp.Width * -Math.Cos(x_coor) + bmp.Height * Math.Sin(y_coor));
                newHeight = (int)(bmp.Height * -Math.Cos(x_coor) + bmp.Width * Math.Sin(y_coor));
                red = 127; blue = 127; green = 255;
            }
            else if (angle > 180 && angle <= 270)
            {
                newWidth = (int)(bmp.Width * -Math.Cos(x_coor) + bmp.Height * -Math.Sin(y_coor));
                newHeight = (int)(bmp.Height * -Math.Cos(x_coor) + bmp.Width * -Math.Sin(y_coor));
                red = 76; blue = 76; green = 255;
            }
            else if (angle > 270 && angle <= 360)
            {
                newWidth = (int)(bmp.Width * Math.Cos(x_coor) + bmp.Height * -Math.Sin(y_coor));
                newHeight = (int)(bmp.Height * Math.Cos(x_coor) + bmp.Width * -Math.Sin(y_coor));
                red = 0; blue = 0; green = 255;
            }

            return new Tuple<int, int, int, int, int>(newWidth, newHeight, red, blue, green);
        }
    }
}
